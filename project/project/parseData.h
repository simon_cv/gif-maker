#ifndef PARSEDATA
#define PARSEDATA
#include "linkedList.h"
#include <stdio.h>

/*function that parses the data
input -
pointer to file
len of list
output -
first node of the new list
*/
FrameNode* parseData(FILE* project, int* listLen);

/*function that find size of file
input -
address of file
output -
size of file
*/
int findSize(FILE* file);

/*function that parses data into node
input -
token - the line to parse
output -
address of node
*/
Frame* parseDataIntoNode(char* buffer);

/*function that saves the project 
input - 
first - the first node in the list
listLen - len of list
output-
none
*/
void saveProject(FrameNode** first);
#endif // PARSEDATA
