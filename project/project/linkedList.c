#include "linkedList.h"
#include "basics.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NULL_BYTE 1


FrameNode* createNode(void)
{
	FILE* path = NULL;
	FrameNode* frame = NULL;
	Frame* node = (Frame*)malloc(sizeof(Frame));
	char dur[STR_LEN] = { 0 };

	node->path = (char*)malloc(STR_LEN * sizeof(char));
	node->name = (char*)malloc(STR_LEN * sizeof(char));
	
	printf("*** Creating new frame ***\nPlease insert frame path :\n");
	myFgets(node->path);
	node->path = realloc(node->path, strlen(node->path) + NULL_BYTE);
	
	printf("Please insert frame duration(in miliseconds):\n");
	scanf("%d", &(node->duration));
	getchar();	

	printf("Please choose a name for that frame:\n");
	myFgets(node->name);
	node->name = realloc(node->name, strlen(node->name) + NULL_BYTE);
	
	frame = createFrameNode(node);
	path = fopen(frame->frame->path, "rb");
	
	if (!path)
	{
		printf("Can't find file! Frame will not be added\n\n");
		freeNodes(&frame);
		frame = NULL;
	}
	else
	{
		fclose(path);
	}
	
	return frame;
}

void freeNodes(FrameNode** first)
{
	FrameNode* curr = NULL;
	
	if (*first)
	{
		curr = *first;
	
		while (curr)
		{
			*first = (*first)->next;
			free(curr->frame->name);
			free(curr->frame->path);
			free(curr->frame);
			free(curr);
			curr = *first;
		}
	}
}

void connectNodes(FrameNode** first, FrameNode* node)
{
	FrameNode* curr = NULL;

	if(!*first)
	{
		*first = node;
	}

	else
	{
		curr = (*first);

		while (curr->next)
		{
			curr = curr->next;
		}

		curr->next = node;
	}
}

void removeNode(FrameNode** first,int* listLen)
{
	char name[STR_LEN] = { 0 };
	FrameNode* curr = NULL;
	FrameNode* p = NULL;
	int nameFound = 0;

	if (*first)
	{
		curr = *first;

		printf("Enter the name of the frame you wish to erase\n");
		myFgets(name);

		if (!strcmp(curr->frame->name, name))//if name is  in the head of the list
		{
			curr = NULL;
			nameFound = 1;
		}	

		while (!nameFound && curr->next)//checking rest of the list
		{
			if (!strcmp(name, curr->next->frame->name))
			{
				//curr = curr->next;
				nameFound = 1;
			}
			else
			{
				curr = curr->next;
			}	
		}
		if (!nameFound)
		{
			printf("The frame was not found\n");
		}
		else
		{
			if (curr == NULL)//node is first
			{
				curr = *first;
				*first = (*first)->next;
				curr->next = NULL;
			}
			else if (!curr->next->next)//node is last
			{
				p = curr;
				curr = curr->next;
				p->next = NULL;
				curr->next = NULL;
			}
			else//node is in the rest of the list 
			{
				p = curr;
				curr = curr->next;
				p->next = curr->next;
				curr->next = NULL;
			}
			freeNodes(&curr);
			printf("%s removed from list", name);
			*listLen -= 1;
		}
	}
	else
	{
		printf("List is empty!\n");
	}
}

FrameNode* createFrameNode(Frame* node)
{
	FrameNode* frame = (FrameNode*)malloc(sizeof(FrameNode));
	frame->frame = node;
	frame->next = NULL;
	
	return frame;
}

void changeFrameIndex(FrameNode** first, int listLen)
{
	char name[STR_LEN] = { 0 };
	int index = 0;
	int counter = 0;
	int frameFound = 0;
	FrameNode* frameToMove = NULL;
	FrameNode* nodeToReplace = NULL;
	FrameNode* pFrameToMove = NULL;
	FrameNode* pNodeToReplace = NULL;
	FrameNode* curr = NULL;
	
	printf("Enter the name of the frame\n");
	myFgets(name);
	
	printf("Enter the new index in the movie you wish to place the frame\n");
	scanf("%d", &index);
	getchar();
	
	if (listLen >= index)
	{
		curr = *first;
	
		if (!strcmp(name, curr->frame->name))//name found in the first node
		{
			frameFound = 1;
			frameToMove = curr;
		}
		while (curr->next && (!nodeToReplace || !frameToMove))
		{
			counter += 1;
		
			if (!strcmp(name, curr->next->frame->name) && !frameFound)
			{
				frameFound = 1;
				pFrameToMove = curr;
				frameToMove = curr->next;
			}
			if (counter + 1 == index)//finding the node before the index that was picked
			{
				pNodeToReplace = curr;
				nodeToReplace = pNodeToReplace->next;
			}
			else if(counter == index)//if the node to replace is the first one
			{
				nodeToReplace = curr;
			}
			
			curr = curr->next;
		}
		if (!frameFound)
		{
			printf("this frame does not exist\n");
		}
		else
		{
			if (nodeToReplace != frameToMove)
			{
				swapNodes(frameToMove, nodeToReplace, first, pFrameToMove, pNodeToReplace);
			}
		}
	}
	else
	{
		printf("There are only %d frames\n", listLen);
	}
}

void swapNodes(FrameNode* frameToMove, FrameNode* nodeToReplace, FrameNode** first, FrameNode* pFrameToMove, FrameNode* pNodeToReplace)
{

	if (frameToMove == *first)//if the frame to move is the head of the list
	{
		*first = frameToMove->next;
		frameToMove->next = nodeToReplace->next;
		nodeToReplace->next = frameToMove;
	}
	else if(nodeToReplace == *first)//if the node to replace is the head of the list
	{
		pFrameToMove->next = frameToMove->next;
		frameToMove->next = nodeToReplace;
		*first = frameToMove;
	}
	else//if none of the nodes to replace are the head of the list
	{
		pFrameToMove->next = frameToMove->next;
		pNodeToReplace->next = frameToMove;
		frameToMove->next = nodeToReplace;
	}
}

void changeFrameDuartion(FrameNode** first)
{
	int newDuration = 0;
	char name[STR_LEN] = { 0 };
	FrameNode* curr = NULL;	
	
	printf("enter the name of the frame\n");
	myFgets(name);

	printf("Enter the new duration\n");
	scanf("%d", &newDuration);

	curr = *first;

	while (curr && strcmp(name,curr->frame->name))
	{
		curr = curr->next;
	}
	if (!curr)
	{
		printf("Frame not found");
	}
	else
	{
		curr->frame->duration = newDuration;
	}

	printf("\n\n");
}

void changeAllDuration(FrameNode** first)
{
	FrameNode* curr = NULL;
	int newDuration = 0;

	printf("Enter the duration for all frames:\n");
	scanf("%d", &newDuration);

	if(*first)
	{
		curr = *first;
		
		while (curr)
		{
			curr->frame->duration = newDuration;
			curr = curr->next;
		}
	}
	else
	{
		printf("List is empty");
	}
	printf("\n\n");
	}
	
void listFrames(FrameNode** first)
{
	FrameNode* curr = NULL;
	
	if (*first)
	{
		printf("%-10s\t%-10s\t%-20s\n", "Name", "Duration", "Path");
		curr = *first;
		
		while (curr)
		{
			printf("%-10s\t%-10d\t%-20s\n", curr->frame->name, curr->frame->duration, curr->frame->path);
			curr = curr->next;
		}
	}
	printf("\n\n");
}

FrameNode* reverseList(FrameNode** first)
{
	FrameNode* newFirst = NULL;
	FrameNode* newNode = NULL;
	FrameNode* curr = NULL;
	if (*first)
	{
		curr = *first;
		while (curr)
		{
			newNode = (FrameNode*)malloc(sizeof(FrameNode));
			newNode->frame = curr->frame;
			newNode->next = NULL;
			newNode->next = newFirst;
			newFirst = newNode;
			*first = (*first)->next;
			free(curr);
			curr = *first;
			
		}
		
	}
	return newFirst;
}
