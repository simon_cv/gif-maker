/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:simon                     *
* Credits:                       *
**********************************/

#include <stdio.h>
#include <stdlib.h>
#include "basics.h"
#include "linkedList.h"
#include "view.h"
#include "parseData.h"

int main(void)
{
	int listLen = 0;
	FrameNode* first = NULL;
	FrameNode* frame = NULL;
	int choice = 0;
	FILE* project = NULL;
	char* path = NULL;
	int innerChoice = 1;

	choice = startMenu();

	if(choice)//if user asked to load a project
	{
		path = getPath();		
		project = fopen(path, "r");
		if (!project)
		{
			printf("Error!- cant open file, creating a new project\n");
		}
		else
		{
			first = parseData(project,&listLen);
			fclose(project);
		}
		free(path);
	}

	while (innerChoice)
	{
		innerChoice = printMenu();
		switch (innerChoice)
		{
		case 1:
			frame = createNode();
			connectNodes(&first, frame);
			listLen += 1;
			break;
		case 2:
			removeNode(&first, &listLen);
			break;
		case 3:
			changeFrameIndex(&first, listLen);
			break;
		case 4:
			changeFrameDuartion(&first);
			break;
		case 5:
			changeAllDuration(&first);
			break;
		case 6:
			listFrames(&first);
			break;
		case 7:
			if (first)
			{
				play(first);
			}
			else
			{
				printf("List is empty!\n");
			}
			break;
		case 8:
			saveProject(&first);
			break;
		case 9:
			first = reverseList(&first);
			if(first)
			{
				play(first);
			}
			else
			{
				printf("List is empty!\n");
			}
			first = reverseList(&first);
			break;
		}
	}
	freeNodes(&first);
	getchar();
	return 0;
}