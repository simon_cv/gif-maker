#ifndef LINKEDLISTH
#define LINKEDLISTH

#define FALSE 0
#define TRUE !FALSE
#define STR_LEN 200


// Frame struct
typedef struct Frame
{
	char*		name;
	unsigned int	duration;	
	char*		path;  
} Frame;


// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;

/*function that creates frame
input -
none
output -
address of Framenode
*/
FrameNode* createNode(void);

/*function that frees the nodes
input -
addrres of pointer to the first node
output-
none
*/
void freeNodes(FrameNode** first);

/*function that connects the nodes
input -
first - the first node in the list
output- 
none
*/
void connectNodes(FrameNode** first, FrameNode* node);

/*function that removes the frame
input - 
first - the first node of the list
liistLen - len of list
output - none
*/
void removeNode(FrameNode** first, int* listLen);

/*function that creates a FrameNode
input-
node - the node to include in the FrameNode
output -
addres of FrameNode node
*/
FrameNode* createFrameNode(Frame* node);

/*function that swaps nodes
input-
first - the first node in the list 
listLen - the len of the list
output-
none
*/
void changeFrameIndex(FrameNode** first, int listLen);

/*function that does the swapping action between the nodes
input-
frameToMove - the node to move
nodeToReplace - the node to replace with the node to move to
first - the first node in the list
pframeToMove - the node previous to the node to move
pNodeToReplace - the node previous to the node to replace with the node to move to
output-
none
*/
void swapNodes(FrameNode* frameToMove, FrameNode* nodeToReplace, FrameNode** first, FrameNode* pFrameToMove, FrameNode* pNodeToReplace);

/*function that changes frame furation
input-
first - first node in the list
output-
none
*/
void changeFrameDuartion(FrameNode** first);

/*function that changes the duration of all of the frames
input-
first
output-
none
*/
void changeAllDuration(FrameNode** first);

/*function that lists all of the frames in the proogram
input-
first- the first node in the list
output-
none
*/
void listFrames(FrameNode** first);

/*function that reverses the list
input-
first - the head of the list
output-
the head of the reversed list
*/
FrameNode* reverseList(FrameNode** first);

#endif

