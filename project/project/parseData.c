#include "parseData.h"
#include "linkedList.h"
#include "basics.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NULL_BYTE 1

FrameNode* parseData(FILE* project, int* listLen)
{
	FrameNode* first = NULL;
	Frame* node = NULL;
	FrameNode* frame = NULL;
	char* fileContent = NULL;
	char* buffer = (char*)malloc(sizeof(char) * STR_LEN + NULL_BYTE);
	int fileSize = findSize(project);
	while (ftell(project) < fileSize)
	{
		fgets(buffer, STR_LEN, project) != 0;
		buffer[strcspn(buffer, "\n")] = 0;
		node = parseDataIntoNode(buffer);
		frame = createFrameNode(node);
		connectNodes(&first, frame);
		*listLen += 1;
	}
	
	free(buffer);
	
	return first;
}

int findSize(FILE* file)
{
	int size = 0;
	
	fseek(file,0,SEEK_END);
	size = ftell(file);
	fseek(file,0,SEEK_SET);
	
	return size;
}

Frame* parseDataIntoNode(char* buffer)
{
	char* token = NULL;
	Frame* node = (Frame*)malloc(sizeof(Frame));
	
	token = strtok(buffer, "-");// "-" is delimiter
	
	node->name = (char*)malloc(sizeof(char) * (strlen(token) + NULL_BYTE));
	strncpy(node->name, token, strlen(token));
	node->name[strlen(token)] = 0;//putting NULL BYTE manually because strncpy doesnt put it
	
	token = strtok(NULL, "-");
	node->duration = atoi(token);
	token = strtok(NULL, "-");
	
	node->path = (char*)malloc(sizeof(char) * (strlen(token) + NULL_BYTE));
	strncpy(node->path, token, strlen(token));
	node->path[strlen(token)] = 0;//putting NULL BYTE manually because strncpy doesnt put it

	return node;
}

void saveProject(FrameNode** first)
{
	FrameNode* curr = NULL;
	FILE* project = NULL;
	char* buffer = NULL;
	char path[STR_LEN] = {0};
	
	if (*first)
	{
		curr = *first;
		
		printf("Where to save the project? enter a full path and file name\n");
		myFgets(path);
		
		project = fopen(path, "w");
	
		if (!project)
		{
			printf("Error! canot create file\n");
		}		
		else
		{
			while (curr)//writing the data into file
			{
				buffer = curr->frame->name;
				fwrite(buffer, sizeof(char), strlen(buffer), project);
				fwrite("-", sizeof(char), strlen("-"), project);
				_itoa(curr->frame->duration, buffer, 10);
				fwrite(buffer, sizeof(char), strlen(buffer), project);
				fwrite("-", sizeof(char), strlen("-"), project);
				buffer = curr->frame->path;
				fwrite(buffer, sizeof(char), strlen(buffer), project);
				buffer = "\n";
				fwrite(buffer, sizeof(char), strlen(buffer), project);
				curr = curr->next;
			}
			printf("Project saved!\n\n");
			fclose(project);
		}
	}
}