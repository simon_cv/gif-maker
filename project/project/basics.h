#ifndef BASICS
#define BASICS

/*function that prints the start menu and gets input
input-
none
output-
choice
*/
int startMenu(void);

/*function that gets path input
input-
none
output-
path
*/
char* getPath(void);


/*function that prints the second menu and gets choice
input-
none
output-
choice
*/
int printMenu(void);

/*function that removes \n from string
input-
string
output -
none
*/
void myFgets(char* arr);

#endif // !BASICS
