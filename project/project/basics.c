#include "basics.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_LEN 200
#define NULL_SIZE 1


int startMenu(void)
{
	int choice = 0;
	
	printf("***** VIDEOINPUT LIBRARY - 0.1995 - TFW07 *****\n\nWelcome to Magshimim Movie Maker! what would you like to do?\n [0] Create a new project\n [1] Load existing project\n");
	scanf("%d",&choice);
	getchar();
	
	while (choice > 1 || choice < 0)
	{
		printf("Welcome to Magshimim Movie Maker! what would you like to do?\n [0] Create a new project\n [1] Load existing project\n");
		scanf("%d", &choice);
		getchar();
	}
	
	return choice;
}

char* getPath(void)
{
	char* path = NULL;

	path = (char*)malloc(sizeof(char) * STR_LEN);
	printf("Enter the path of the project (including project name):\n");
	myFgets(path);
	path = (char*)realloc(path, strlen(path) + NULL_SIZE);

	return path;
}


void myFgets(char* arr)
{
	fgets(arr, STR_LEN, stdin);
	arr[strcspn(arr, "\n")] = 0;
}


int printMenu(void) 
{
	int choice = 0;

	printf("What would you like to do?\n [0] Exit\n [1] Add new Frame\n [2] Remove a Frame\n [3] Change frame index\n [4] Change frame duration\n [5] Change duration of all frames\n [6] List frames\n [7] Play movie!\n [8] Save project\n [9] Play in reverse\n");
	scanf("%d", &choice);
	getchar();

	while (choice > 9 || choice < 0)
	{
		printf("What would you like to do?\n [0] Exit\n [1] Add new Frame\n [2] Remove a Frame\n [3] Change frame index\n [4] Change frame duration\n [5] Change duration of all frames\n [6] List frames\n [7] Play movie!\n [8] Save project\n [9] Play in reverse\n");
		scanf("%d", &choice);
		getchar();
	}

	return choice;
}